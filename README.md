# Anon's unnamed puzzle game

## Getting the game
You can download the source code and run it in Godot yourself (tested under Godot 3.2), or you can look in the export directory for binaries. Links for the lazy:

- [Windows](export/windows.zip)
- [*nix](export/x11.tar.gz)

## How to play
[Here is anon's original design.](anonsidea.png) Check it out for the basic idea on how the game works.

Use the arrow keys, Z, and X to navigate the menus. It should be obvious what each choice does.

In the game itself, use A/S/D/Z/X/C to place a panel with your current color. The top three keys place it on the top-left, top-middle, and top-right slices, respectively; similar deal with the bottom three.

A feature not specified by the original design is a NEXT window, allowing you to view the next color and plan ahead. The game will also make a sound corresponding to which color is next. You can turn this off if it's annoying.

The timer over your current color shows you how much time you have to make a move. As you clear more panels, the amount of time you have will gradually decrease. If you run out of time, the game will place it at on the slice you last placed a panel at.

### Scoring
Clearing a group of three panels scores 30 points. Clearing an entire ring scores 100.

You can score combos by clearing multiple groups with one move. Clearing one group may cause panels to fall, causing another group to be cleared, and so on. In that case, all the points you gain from that one move are multiplied by an extra 0.5x per group cleared: 1.5x for a combo of 2, 2.0x for 3, 2.5x for 4...
