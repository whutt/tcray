extends Menu

enum {OPT_START, OPT_SCORES, OPT_OPTIONS, OPT_EXIT}

func onReady():
	optArray = ["Start","Scores","Options","Exit"]

func onSelect():
	match cursor:
		OPT_START:
			misc.gotoScene("res://sys/MainGame.tscn")
			close()
		OPT_SCORES:
			misc.gotoScene("res://sys/HighScoreScreen.tscn")
			close()
		OPT_OPTIONS:
			newMenu("res://sys/OptionsMenu.tscn", Vector2(0,0))
		OPT_EXIT:
			get_tree().quit()

func onCancel():
	pass
