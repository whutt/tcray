extends Menu

enum {
	OPT_LABEL,
	OPT_SLIDER,
	OPT_KEYMAP,
	OPT_JOYMAP,
	OPT_LIST,
	OPT_BUTTON
}

var accentCol = Color(0.85,0.85,1)

var keyDescs = {
	"attack": "Talk and use in the field, accept in menus, fire in battle.",
	"card": "Pull up the menu in the field, close out menus, play cards in battle.",
	"speed": "Hold to slow down in battle."
}

enum {
	PAD_STANDARD,
	PAD_XBOX,
	PAD_SONY,
	PAD_NINTENDO
}

var buttonDescs = [
	["A","B","X","Y","LB","RB","LT","RT","L Stick","R Stick","Back","Start"],
	["X","Circle","Square","Triangle","L1","R1","L2","R2","L3","R3","Select","Start"],
	["B","A","Y","X","L","R","? (sorry)","? (sorry)","? (sorry)","? (sorry)","? (sorry)","? (sorry)"]
]
var padKeywords = [
	["xinput"],
	[], [] # don't know keywords for these yet
]
var padMode = PAD_STANDARD

var oldCursor = 0

class Option:
	var id = ""
	var name = ""
	var desc = ""
	var type
	var data = ""
	var value = 0
	static func make(i, n, d, t, c = null, default = 0):
		var op = Option.new()
		op.id = i
		op.name = n
		op.desc = d
		op.type = t
		op.data = c
		op.value = default if not (i in global.settings) else global.settings[i]
		return op

func ok_btnWindowSize():
	var value = getSetting("windowSize")
	global.screenMult = global.RES_AUTO if value == 0 else value
	global.readjustResolution(false)

func change_soundVol(value):
	AudioServer.set_bus_volume_db(sound.busIdx, linear2db(value/100.0))

var settingsList = [
	Option.make("soundVol", "Sound Volume", "Set sound effect volume.", OPT_SLIDER, null, 100),
	Option.make("musicVol", "Music Volume", "Set music volume.", OPT_SLIDER, null, 100),
	Option.make("windowSize", "Window Size", "Set window size. \"Auto\" gives you the best fit for your screen.", OPT_LIST, ["Auto"]),
	Option.make("btnWindowSize", "Change Size Now", "Change the current window size now, so you can test it.", OPT_BUTTON),
	Option.make("messageSpeed", "Message Speed", "Set the speed at which messages fill the dialog box.", OPT_LIST, ["UGH","Slower","Slow","Medium","Fast","Faster","Ridiculous"], 3),
	Option.make("showHitbox", "Show Hitbox", "Set whether your hitbox should be shown in battle.", OPT_LIST, ["Never","When Focused", "Always"], 1),
	Option.make("lblKeyboard", "Keyboard Settings", "", OPT_LABEL),
	Option.make("keyMove", "Movement Keys", "Select a set of directional keys.", OPT_LIST, ["Arrows","WASD","HJKL (vi lol)"]),
	Option.make("keyAttack", "Fire/Accept", "", OPT_KEYMAP, "attack"),
	Option.make("keyCard", "Card/Cancel", "", OPT_KEYMAP, "card"),
	Option.make("keySpeed", "Focus", "", OPT_KEYMAP, "speed"),
	Option.make("lblJoypad", "Gamepad Settings", "", OPT_LABEL),
	Option.make("joyDeadX", "X Deadzone", "Set the deadzones for your gamepad.", OPT_SLIDER, null, 25),
	Option.make("joyDeadY", "Y Deadzone", "Set the deadzones for your gamepad.", OPT_SLIDER, null, 25),
	Option.make("joyAttack", "Fire/Accept", "", OPT_JOYMAP, "attack"),
	Option.make("joyCard", "Card/Cancel", "", OPT_JOYMAP, "card"),
	Option.make("joySpeed", "Focus", "", OPT_JOYMAP, "speed"),
]

var settingsDict = {}

func getSetting(n):
	return settingsDict[n].value

func _ready():
	optArray = []
	
	# detect what kind of pad we're using so we know what to name it
	var joyName = Input.get_joy_name(0)
	print("using pad: ", joyName)
	
	for i in len(padKeywords):
		if padMode > 0:
			break
		for k in padKeywords[i]:
			if joyName.to_lower().find(k) != -1:
				padMode = i+1
				break
	
	for i in settingsList:
		optArray.append(i.name)
		i.desc = i.desc if not (i.type in [OPT_JOYMAP, OPT_KEYMAP]) else keyDescs[i.data]
		settingsDict[i.id] = i
		if i.type == OPT_JOYMAP:
			i.value = global.settings["joy_"+i.data]
		if i.type == OPT_KEYMAP:
			i.value = global.settings["key_"+i.data]
	
	var windowSize = settingsDict["windowSize"]
	var screenSize = OS.get_screen_size()
	screenSize.y -= 64 # should be enough to deal w/ window decorations etc.
	
	for i in range(min(int(screenSize.x / global.SCREEN_W), int(screenSize.y / global.SCREEN_H))):
		var fullRes = "%dx:%dx%d" % [i+1, global.SCREEN_W * (i+1), global.SCREEN_H * (i+1)]
		var pRes = "%dx (%dp)" % [i+1, global.SCREEN_H * (i+1)]
		windowSize.data.append(fullRes if len(fullRes) <= 13 else pRes)
	
func onCursor():
	if settingsList[cursor].type == OPT_LABEL:
		var add = cursor - oldCursor
		jumpCursor(cursor+add)
	oldCursor = cursor

func onCancel():
	var curWindowMode = global.settings["windowSize"] if "windowSize" in global.settings else 0
	for s in settingsList:
		match s.type:
			OPT_SLIDER,OPT_LIST:
				global.settings[s.id] = s.value
			OPT_KEYMAP:
				global.settings["key_"+s.data] = s.value
			OPT_JOYMAP:
				global.settings["joy_"+s.data] = s.value
	
	if global.settings["windowSize"] != curWindowMode:
		ok_btnWindowSize()
	
	close()

func onSelect():
	var setting = settingsList[cursor]
	if setting.type == OPT_BUTTON and has_method("ok_"+setting.id):
		call("ok_"+setting.id)

func drawItem(i: int, j: int, base: Vector2):
	var setting = settingsList[j]
	match setting.type:
		OPT_LABEL:
			drawHeader(Vector2(base.x, base.y) + Vector2(0, (i+1)*itemSpace), setting.name)
		OPT_BUTTON:
			drawItemBasic(i, j, base+Vector2(0, 0), setting.name, accentCol)
			#drawItemBasic(i, j, base, setting.name)
		_:
			drawItemBasic(i, j, base, setting.name)
			var base2 = base+Vector2(114, 0)
			match setting.type:
				OPT_LIST:
					drawItemBasic(i, j, base2, setting.data[setting.value], accentCol)
				OPT_KEYMAP:
					drawItemBasic(i, j, base2, OS.get_scancode_string(setting.value), accentCol)
				OPT_JOYMAP:
					drawItemBasic(i, j, base2, "Button %d" % setting.value if padMode == 0 else buttonDescs[padMode-1][setting.value], accentCol)
				OPT_SLIDER:
					var valSt = "%d%%" % (setting.value)
					var baseAdd = Vector2(36 - len(valSt) * 3, 0)
					#var base3 = base2+Vector2(-4, (i+1)*itemSpace)
					#drawHeader(base3, valSt, accentCol)
					#draw_rect(Rect2(base3, Vector2(44, 4)), accentCol, false)
					drawItemBasic(i, j, base2+baseAdd, valSt, accentCol)
	#drawItemBasic(i, j, base+Vector2(120, 8), senderArray[j])

func onSideCursor(add):
	var setting = settingsList[cursor]
	match setting.type:
		OPT_SLIDER:
			setting.value = max(0, min(setting.value+add*5, 100))
		OPT_LIST:
			setting.value = max(0, min(setting.value+add, len(setting.data)-1))
	
	if has_method("change_"+setting.id):
		call("change_"+setting.id, setting.value)
	update()

func onDraw():
	var size = get_size()+Vector2(18,0)
	var topLeft = Vector2(16, 32)
	
	drawContents(topLeft, size)
	drawItemDescription(settingsList[cursor])
