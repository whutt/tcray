extends Node

enum {
	OPT_LABEL,
	OPT_SLIDER,
	OPT_KEYMAP,
	OPT_JOYMAP,
	OPT_LIST,
	OPT_BUTTON
}

var curSettings = {}
var buttons = ["ui_accept","ui_cancel","a","b","c","d","e","f"]

class Option:
	var id = ""
	var name = ""
	var desc = ""
	var type
	var data = ""
	var value = 0
	static func make(i, n, d, t, c = null, default = 0):
		var op = Option.new()
		op.id = i
		op.name = n
		op.desc = d
		op.type = t
		op.data = c
		op.value = default
		return op

var settingsList = [
	Option.make("soundVol", "Sound Volume", "Set sound effect volume.", OPT_SLIDER, null, 100),
	Option.make("nextSound", "Next Sound", "Play a sound to represent the color in the NEXT window.", OPT_LIST, ["Off","On"], 1),
	Option.make("windowSize", "Window Size", "Set window size. \"Auto\" gives you the best fit for your screen.", OPT_LIST, ["Auto"]),
	Option.make("btnWindowSize", "Change Size Now", "Change the current window size now, so you can test it.", OPT_BUTTON),
	# probably not worth it for such a simple game
	#Option.make("lblKeyboard", "Keyboard Settings", "", OPT_LABEL),
	#Option.make("keyAccept", "Accept", "", OPT_KEYMAP, "ui_accept"),
	#Option.make("keyCancel", "Cancel", "", OPT_KEYMAP, "ui_cancel"),
]

func change_soundVol(value):
	AudioServer.set_bus_volume_db(sound.busIdx, linear2db(value/100.0))

enum {RES_FULLSCREEN = 0, RES_AUTO = -1}
var screenMult = RES_AUTO

func _ready():
	var setFile = "user://settings"
	var file = File.new()
	if file.file_exists(setFile):
		curSettings = misc.loadJSON(setFile)
	else:
		print("settings file does NOT exist")
		for i in settingsList:
			curSettings[i.id] = i.value
		# unfinished stuff from prev project, would need to finish if keymap settings are wanted
		for b in buttons:
			var actions = InputMap.get_action_list(b)
			for a in actions:
				match a.get_class():
					"InputEventKey":
						curSettings["key_"+b] = a.scancode
					"InputEventJoypadButton":
						curSettings["joy_"+b] = a.button_index
					_:
						pass
		saveSettings()
		
	for k in curSettings:
		if has_method("change_"+k):
			call("change_"+k, curSettings[k])
	
	readjustResolution()

func saveSettings():
	misc.saveJSON("user://settings", curSettings)

func readjustResolution(var init = true):
	var mult = RES_AUTO if curSettings["windowSize"] == 0 else curSettings["windowSize"]
	if mult == RES_AUTO:
		var screenSize = OS.get_screen_size()
		screenSize.y -= 64 # should be enough to deal w/ window decorations etc.
		mult = min(int(screenSize.x / misc.SCREEN_W), int(screenSize.y / misc.SCREEN_H))
	if init:
		OS.set_window_position(OS.get_window_position() - misc.screenDims * (mult - 1) * 0.5)
	OS.set_window_size(misc.screenDims * mult)
