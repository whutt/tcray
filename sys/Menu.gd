extends Control
class_name Menu

signal closed

export(String, MULTILINE) var options
export var menuMargin = Vector2(5, 4)
export var openSound = "accept"
export var acceptSound = "accept"
export var cancelSound = "cancel"
export var cursorSound = true
export var cancelable = true
export var horizontal = false
export var sideControls = false
export var alwaysUpdate = false
export var itemSpace = 10
export var animSpeed = 24.0
export var cursorOffset = 0
export var scroll = true
export var scrollSize = 10
export var forceWidth = 0
export var noAutoInit = false

var animTime = 0
var optArray = []
var cursor = 0
var scrollPos = 0
var nowClosing = false
var accepted = false
var noInput = false

onready var cursorTex = preload("res://sprites/menu/menuHilight.png")
onready var head = load("res://sprites/messageHead.png")

func drawCursor(pos, l):
	misc.drawBar(self, cursorTex, 4, 4, pos, l)

func newMenuLoaded(node, pos := Vector2(0,0)):
	pause_mode = PAUSE_MODE_STOP
	if node.get_position() == Vector2(0,0) and pos != Vector2(0,0):
		node.set_position(pos)
	yield(misc.callMenuLoaded(node, misc.MENU_NONROOT), "completed")
	pause_mode = PAUSE_MODE_PROCESS

func newMenu(path, pos := Vector2(0,0)):
	newMenuLoaded(load(path).instance(), pos)

func _ready():
	#get_tree().paused = true
	pause_mode = PAUSE_MODE_PROCESS
	onReady()
	if not noAutoInit:
		reinit()
	playSound(openSound)

func onReady():
	pass

func fromRight(r: Rect2):
	return Vector2(misc.SCREEN_W - (r.position.x + r.size.x), r.position.y)

func reinit():
	if optArray.size() == 0:
		optArray = options.split("\n")
	
	var maxLen = 0
	for st in optArray:
		maxLen = max(misc.mainFont.get_string_size(st).x, maxLen)
	
	if not scroll:
		scrollSize = len(optArray)
	
	var size = Vector2(0, 0)
	var w = forceWidth if forceWidth > 0 else maxLen
	
	size.x = w + menuMargin.x * 2
	size.y = scrollSize * itemSpace + menuMargin.y * 2 + 2 # last number is a juryrig
	set_size(size)

func close():
	nowClosing = true
	emit_signal("closed")

func closing(delta):
	queue_free()

func _draw():
	onDraw()
	
func drawTitle(pos: Vector2, st: String):
	misc.drawBar(self, head, 3, 9, pos, 18 + 6*len(st))
	misc.drawSmall(self, pos+Vector2(6, 11), st)

func drawHeader(pos: Vector2, st: String, col:= Color(0.8, 0.8, 0.8)):
	misc.drawSmall(self, menuMargin+pos+Vector2(8, 0), st, col)

func drawLabel(pos: Vector2, st: String, col:= Color(1, 1, 1)):
	misc.drawString(self, menuMargin+pos, st, col)

func drawItemBasic(i: int, j: int, base: Vector2, st := "", col = Color(1,1,1)):
	st = optArray[j] if st == "" else st
	drawLabel(Vector2(base.x, base.y+(i+1)*itemSpace), st, col)

func drawItem(i: int, j: int, base: Vector2):
	drawItemBasic(i, j, base)

func drawContents(pos: Vector2, size := Vector2(-1, -1)):
	var x = pos.x
	var y = pos.y
	
	if size == Vector2(-1, -1):
		size = get_size()
	
	misc.drawBox(self, Rect2(x, y, size.x, size.y))
	drawCursor(Vector2(x+menuMargin.x - 1, y + cursorOffset + menuMargin.y + (1+cursor-scrollPos) * itemSpace -5), size.x - menuMargin.x * 2 + 2)
	
	for i in range(scrollSize):
		var j = i + scrollPos
		if j < len(optArray):
			#misc.drawString(self, Vector2(x+menuMargin.x, y+(i+1)*10 + menuMargin.y), optArray[j])
			drawItem(i, j, pos)

func drawItemDescription(item):
	misc.drawBox(self, Rect2(0, misc.SCREEN_H-36, misc.SCREEN_W, 36))
	misc.drawString(self, Vector2(6, misc.SCREEN_H-24), misc.wrapText(item.desc))

func onDraw():
	drawContents(Vector2(0,0))
	
func playSound(s: String):
	if s != "":
		sound.play($"/root/sound/defaultVoice", s)

func jumpCursor(p):
	cursor = wrapi(p, 0, len(optArray))
	if cursor < scrollPos:
		scrollPos = cursor
	elif cursor >= scrollPos+scrollSize-1:
		scrollPos = cursor-scrollSize+1

func _process(delta):
	if alwaysUpdate:
		update()
	
	if nowClosing:
		closing(delta)
		return
	
	if noInput:
		return
	var add = 0
	var plusKey = "move_right" if horizontal else "move_down"
	var minusKey = "move_left" if horizontal else "move_up"
	
	var mov = scrollSize if (Input.is_action_pressed("speed") and scroll) else 1
	
	if Input.is_action_just_pressed(plusKey):
		add = mov
	if Input.is_action_just_pressed(minusKey):
		add = -mov
	if add != 0:
		jumpCursor(cursor+add)
		update()
		if cursorSound:
			playSound("cursor")
		onCursor()
	
	if sideControls:
		add = 0
		plusKey = "move_down" if horizontal else "move_right"
		minusKey = "move_up" if horizontal else "move_left"
		if Input.is_action_just_pressed(plusKey):
			add = 1
		if Input.is_action_just_pressed(minusKey):
			add = -1
		if add != 0:
			update()
			if cursorSound:
				playSound("cursor")
			onSideCursor(add)
	
	if Input.is_action_just_pressed("ui_accept"):
		if acceptSound != "":
			playSound(acceptSound)
		onSelect()
	
	if Input.is_action_just_pressed("ui_cancel") and cancelable:
		if cancelSound != "":
			playSound(cancelSound)
		onCancel()

func onCursor():
	pass

func onSideCursor(add):
	pass

func onCancel():
	close()

func onSelect():
	#newMenu("res://sys/Menu.tscn", get_position()+Vector2(16, 16))
	accepted = true
	close()
