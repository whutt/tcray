extends Node2D

enum {STATE_READY, STATE_INPUT, STATE_FLASH, STATE_DEATH}

var curState = STATE_READY
var stateTime = 0.0
var curCombo = 0
var comboScore = 0
var lastCombo = 0
var lastTime = 0.0
var placeTimer = 0.0
var placeMax = 6.0
var cursor = 1

var field = []

var radius = misc.SCREEN_H / 2 - 10
var center = Vector2(misc.SCREEN_W, misc.SCREEN_H) / 2
var innerRad = 10
var nextPos = Vector2(misc.SCREEN_W - innerRad - 3, 10 + innerRad)

var fieldImg = Image.new()
var fieldTex = ImageTexture.new()

var gameID = OS.get_unix_time()
var score = 0
var totalClears = 0
var totalTime = 0.0

var rng = RandomNumberGenerator.new()

var colors = [Color(0,0,0,0), Color.red, Color.yellow, Color.green, Color.cyan, Color.blue, Color.magenta]
var curColor = 0
var nextColor = 0

var toClear = []
var buttons = "cbadef"

func _draw():
	draw_circle(center, radius, Color(1,1,1,0.125))
	draw_circle(center, innerRad, colors[curColor] if curState != STATE_DEATH else Color(0.5,0.5,0.5))
	
	misc.drawSmall(self, Vector2(1, 9), "SC")
	var pos = int(log(max(score, 1))/log(10))
	misc.drawSmall(self, Vector2(19, 9), "0".repeat(5-pos), Color(0.5, 0.5, 0.5))
	misc.drawSmall(self, Vector2(19+30-6*pos, 9), str(score), Color(0.9, 0.9, 0.9))
	
	if lastTime > 0.0:
		var lastSt = "+%d" % lastCombo
		misc.drawSmall(self, Vector2(19+36-6*len(lastSt), 17), str(lastSt), Color(1,1,1,sqrt(lastTime)))
	
	misc.drawSmall(self, Vector2(1, 25), "CLEARS")
	pos = int(log(max(totalClears, 1))/log(10))
	misc.drawSmall(self, Vector2(31-6*pos, 33), str(totalClears), Color(0.9, 0.9, 0.9))
	misc.drawSmall(self, Vector2(misc.SCREEN_W - 6 * 4, 9), "NEXT")
	draw_circle(nextPos, innerRad, colors[nextColor])
	misc.drawSmall(self, Vector2(misc.SCREEN_W - 6 * 4, misc.SCREEN_H - 8), "TIME")
	var format = misc.parseTime(totalTime)
	misc.drawSmall(self, Vector2(misc.SCREEN_W - 6 * len(format), misc.SCREEN_H), format)
	

func _ready():
	rng.randomize()
	fieldImg.create(8, 8, false, Image.FORMAT_RGBA8)
	fieldImg.fill(Color(0,0,0,0))
	
	fieldTex.create_from_image(fieldImg, 0)
	$fieldSprite.material.set_shader_param("tex", fieldTex)
	
	fieldTex.set_data(fieldImg)
	
	$grid.innerRad = innerRad
	$grid.radius = radius
	$grid.center = center
	$grid.nextPos = nextPos
	
	for _i in range(6):
		field.append([0,0,0,0,0,0,0])
	
	
	
	call_deferred("callReady")
	#newColor()
	refreshField()

func callReady():
	misc.callMenu("res://sys/ReadyThing.tscn")

func _process(delta):
	update()
	totalTime += delta if not (curState in [STATE_READY,STATE_DEATH]) else 0
	lastTime -= delta
	$fieldSprite.material.set_shader_param("placeTimer", placeTimer / placeMax)
	$fieldSprite.material.set_shader_param("cursor", float(cursor))
	match curState:
		STATE_READY:
			stateTime += delta
			if stateTime >= 0.75:
				setState(STATE_INPUT)
				newColor()
		STATE_INPUT:
			placeTimer += delta * (1.0 + 0.005 * totalClears)
			if placeTimer > placeMax:
				pushCommand(cursor)
			
			for i in range(6):
				var button = buttons[i]
				if Input.is_action_just_pressed(button):
					pushCommand(i)
					break
		STATE_FLASH:
			stateTime += delta * 2.0
			$fieldSprite.material.set_shader_param("flashTime", stateTime)
			if stateTime >= 1.0:
				collapse()
		STATE_DEATH:
			$overlay.update()
			if stateTime < 0.5 and stateTime + delta >= 0.5:
				playSound($blipVoice, "gameover")
			if stateTime > 2.5:
				misc.regScore(score, totalClears, totalTime, gameID)
				misc.gotoScene("res://sys/HighScoreScreen.tscn")
			stateTime += delta
			$fieldSprite.material.set_shader_param("dead", stateTime)

func pushCommand(pos):
	pushColor(pos, curColor)
	cursor = pos
	curColor = 0
	refreshField()
	checkField()
	placeTimer = 0

func pushColor(pos, value):
	playSound($moveVoice, "place")
	field[pos].pop_back()
	field[pos].push_front(value)

func newColor():
	curColor = rng.randi_range(1, 6) if nextColor == 0 else nextColor
	nextColor = rng.randi_range(1, 6)
	if settings.curSettings["nextSound"] > 0:
		playSound($blipVoice, "next"+(" rygcbm"[nextColor]))

func refreshField():
	fieldImg.lock()
	for x in range(6):
		for y in range(7):
			fieldImg.set_pixel(x, y, colors[field[x][y]])
	fieldImg.unlock()
	
	fieldTex.set_data(fieldImg)

func setFlashes(flashes):
	fieldImg.lock()
	for c in range(6):
		fieldImg.set_pixel(c, 7, Color((flashes[c]+0.5)/128.0, 0, 0))
	fieldImg.unlock()
	
	fieldTex.set_data(fieldImg)

func collapse():
	for c in toClear:
		field[c[0]][c[1]] = 0
		
	totalClears += len(toClear)
	
	toClear.clear()
	setFlashes([0,0,0,0,0,0])
	
	var playDropCheck = 0
	var playDrop = false
	
	# inefficient but idc
	for x in range(6):
		var y = 0
		playDropCheck = 0 if playDropCheck == 1 else playDropCheck
		while y < len(field[x]):
			if field[x][y] == 0:
				field[x].remove(y)
				playDropCheck = max(1, playDropCheck)
			else:
				y += 1
				playDropCheck = 2 if playDropCheck == 1 else playDropCheck
		while len(field[x]) < 7:
			field[x].append(0)
		playDrop = playDrop || (playDropCheck == 2)
	
	if playDrop:
		playSound($moveVoice, "drop")
	
	refreshField()
	setState(STATE_INPUT)
	checkField()

func checkField():
	var simulClears = 0
	for y in range(7):
		var rad = [field[0][y] > 0 and field[0][y] == field[2][y] and field[2][y] == field[4][y], 
				   field[1][y] > 0 and field[1][y] == field[3][y] and field[3][y] == field[5][y]]
		# circle
		if rad[0] and rad[1] and field[0][y] == field[1][y]:
			for x in range(6):
				toClear.append([x, y])
			simulClears += 1 
			comboScore += 100 # more pts per panel since these can't create combos
			continue
		
		# "radiation" pattern
		var scored = false
		for i in range(2):
			if rad[i]:
				scored = true
				for j in range(3):
					if field[ (1-i) + j * 2 ][y] == field[i][y]:
						scored = false
						break
				if scored:
					simulClears += 1
					comboScore += 30
					for j in range(3):
						toClear.append([i + j * 2, y])
		if scored:
			continue
		
		# fan (exactly 3 in a row)
		var consec = 0
		var last = 0
		for x in range(1,8):
			#if x == 6 and consec == 0:
			#	break
			var xp = x % 6
			if field[xp][y] > 0 and field[xp][y] == field[last][y]:
				consec += 1
				if consec == 2:
					break
			else:
				consec = 0
				last = xp
		
		if consec == 2:
			consec = 0
			scored = true
			for x in range(3):
				var xp = (last + 3 + x) % 6
				if field[xp][y] == field[last][y]:
					scored = false
					break
				elif field[xp][y] > 0 and field[(xp+3) % 6][y] == field[(last+3) % 6][y]:
					consec += 1
			if scored:
				simulClears += 2 if consec == 3 else 1
				comboScore += 60 if consec == 3 else 30
				
				for x in range(6 if consec == 3 else 3):
					toClear.append([(last + x) % 6, y])
					
		
	if len(toClear) > 0:
		setState(STATE_FLASH)
		curCombo += simulClears
		if curCombo > 1:
			playSound($blipVoice, "combo%d" % min(curCombo, 5))
		playSound($moveVoice, "clear%d" % (3 if simulClears < 2 else 6))
		var clears = [0,0,0,0,0,0]
		for c in toClear:
			clears[c[0]] += 1 << c[1]
		
		setFlashes(clears)
	else:
		newColor()
		if curCombo > 0:
			comboScore = comboScore * (0.5 + 0.5 * curCombo)
			score += comboScore
			
			lastCombo = comboScore
			lastTime = 1.0
			
			curCombo = 0
			comboScore = 0
			
		# death check
		for x in range(6):
			if field[x][6] > 0:
				setState(STATE_DEATH)

func playSound(vox, soundName):
	sound.play(vox, soundName)

func setState(state):
	curState = state
	stateTime = 0.0
