extends Node

# fair warning: a lot of this is vestigial stuff from another project

enum {
	MENU_NONROOT,
	MENU_ROOT
}

var SCREEN_W = 240
var SCREEN_H = 180

var screenRect = Rect2(0, 0, SCREEN_W, SCREEN_H)
var screenDims = Vector2(SCREEN_W, SCREEN_H)

var currentScene = null
var linePos = 120

var sceneStack = []

var fieldScene = null

var numberFont = load("res://fonts/nummern2.font")
var mainFont = load("res://fonts/mainfontVW.font")
var smallFont = load("res://fonts/namefont.font")

var box = load("res://sprites/boxBasic.png")

# juryrig idc
var lastGameID = -1

# score table kept in simple arrays for easy jsoning
# format: score, clears, time, game id
var highScores = []

func highScoreComp(a, b):
	return a[0] > b[0]

func parseTime(t):
	return "%02d:%02d.%02d" % [int(t / 60), int(fmod(t, 60)), int(fmod(t, 1.0) * 100)]

func regScore(score, clears, time, id):
	lastGameID = id
	print(lastGameID)
	highScores.append([score, clears, time, id])
	highScores.sort_custom(self, "highScoreComp")
	highScores.resize(10)
	
	misc.saveJSON("user://hiscores", highScores)

func _ready():
	var root = get_tree().get_root()
	currentScene = root.get_child( root.get_child_count() -1 )
	OS.set_window_title("anon's polar puzzle game")
	
	var file = File.new()
	if file.file_exists("user://hiscores"):
		highScores = loadJSON("user://hiscores")
		for i in range(len(highScores)):
			for j in [0,1,3]:
				highScores[i][j] = int(highScores[i][j])
	else:
		for _i in range(10):
			highScores.append([0,0,0.0,0])
	
	call_deferred("nodeSetup")

func nodeSetup():
	var guiLayer = load("res://sys/guiLayer.tscn").instance()
	$"/root".add_child(guiLayer)
	
	var fxLayer = CanvasLayer.new()
	fxLayer.layer = 11
	fxLayer.name = "fxLayer"
	
	var fxRoot = Control.new()
	fxRoot.name = "fxRoot"
	fxLayer.add_child(fxRoot)
	
	$"/root".add_child(fxLayer)

func callMenuLoaded(menu, root = MENU_NONROOT):
	var isRoot = root == MENU_ROOT
	if isRoot:
		get_tree().paused = true
	
	$"/root/guiLayer/guiRoot".add_child(menu)
	yield(menu, "closed")
	
	if isRoot:
		get_tree().paused = false

func callMenu(path, root = MENU_NONROOT, pos := Vector2(0, 0)):
	var menu = load(path).instance()
	menu.set_position(pos)
	
	yield(callMenuLoaded(menu, root), "completed")
	
func getDict(d, k, fail = null):
	return d[k] if k in d else fail
	
func gotoScene(path):
	call_deferred("_deferred_goto_scene",path)

func pushSceneStack(node):
	#var root = get_node("/root")
	sceneStack.append(currentScene)
	$"/root".remove_child(currentScene)
	currentScene = node
	$"/root".add_child(node)
	#print($"/root".get_children())

func popSceneStack():
	print("going from ", currentScene.name, " to ", sceneStack.back().name)
	$"/root".remove_child(currentScene)
	currentScene.call_deferred("queue_free")
	
	$"/root".add_child(sceneStack.back())
	currentScene = sceneStack.pop_back()

func fadeTransition(node = null):
	return transition("FadeTransition", node)

func transition(trName: String, node = null):
	var tr = load("res://sys/tr/"+trName+".tscn").instance()
	tr.nextScene = node
	$"/root/fxLayer/fxRoot".add_child(tr)
	return tr

func drawStringRaw(font: Font, obj, pos: Vector2, st: String,  color := Color(1, 1, 1)):
	var strings = st.split("\n")
	
	for i in len(strings):
		var s = strings[i]
		obj.draw_string(font, pos + Vector2(1, 1+10*i), s, Color(0,0,0,color.a))
		obj.draw_string(font, pos + Vector2(0, 10*i), s, color)

func drawString(obj, pos: Vector2, st: String, color := Color(1, 1, 1)):
	drawStringRaw(mainFont, obj, pos, st, color)

func drawSmall(obj, pos: Vector2, st: String, color := Color(1, 1, 1)):
	drawStringRaw(smallFont, obj, pos, st, color)

func drawBox(obj, rect: Rect2):
	var x = rect.position.x
	var y = rect.position.y
	var w = rect.size.x
	var h = rect.size.y
	
	# center
	obj.draw_texture_rect_region(box, rect.grow_individual(-2,-2,-2,-2), Rect2(2, 2, 20, 20))
	
	for i in range(2):
		# sides
		obj.draw_texture_rect_region(box, Rect2(x + i * (w-2), y+2, 2, h-4), Rect2(0+i*22, 2, 2, 20))
		obj.draw_texture_rect_region(box, Rect2(x+2, y + i * (h-2), w-4, 2), Rect2(2, 0+i*22, 20, 2))
		
		# corners
		for j in range(2):
			obj.draw_texture_rect_region(box, Rect2(x + i * (w-2), y + j * (h-2), 2, 2), Rect2(0+i*22, 0+j*22, 2, 2))
	
func drawBar(obj, tex: Texture, leftW: int, rightW: int, pos: Vector2, length: int, top := 0, h := -1, col := Color(1, 1, 1)):
	h = h if h != -1 else tex.get_height()
	var w = tex.get_width()
	#var h = tex.get_height()
	obj.draw_texture_rect_region(tex, Rect2(pos+Vector2(leftW, 0), Vector2(length-leftW-rightW, h)), Rect2(leftW, top, w-leftW-rightW, h), col)
	obj.draw_texture_rect_region(tex, Rect2(pos+Vector2(0, 0), Vector2(leftW, h)), Rect2(0, top, leftW, h), col)
	obj.draw_texture_rect_region(tex, Rect2(pos+Vector2(length-rightW, 0), Vector2(rightW, h)), Rect2(w-rightW, top, rightW, h), col)

func wrapText(st: String, lineLimit := 230):
	var split = st.split(" ", false)
	var work = PoolStringArray()
	var targetStrings = PoolStringArray()
	
	for i in range(len(split)):
		if mainFont.get_string_size(work.join(" ") + " " + split[i]).x >= lineLimit:
			targetStrings.append(work.join(" "))
			work.resize(0)
		
		work.append(split[i])
	
	if len(work) > 0:
		targetStrings.append(work.join(" "))
	
	return targetStrings.join("\n")

func loadJSON(path):
	var file = File.new()
	file.open(path, File.READ)
	var st = file.get_as_text()
	file.close()
	return JSON.parse(st).result

func saveJSON(path, data):
	print(OS.get_user_data_dir())
	var file = File.new()
	file.open(path, File.WRITE)
	file.store_string(JSON.print(data))
	file.close()

func isFuncState(obj):
	return typeof(obj) == TYPE_OBJECT and obj.get_class() == "GDScriptFunctionState"

# yoink'd from godot's documentation
func _deferred_goto_scene(path):
	# Immediately free the current scene,
	# there is no risk here.
	currentScene.free()

	# Load new scene
	var s = ResourceLoader.load(path)

	# Instance the new scene
	currentScene = s.instance()

	# Add it to the active scene, as child of root
	get_tree().get_root().add_child(currentScene)

	# optional, to make it compatible with the SceneTree.change_scene() API
	get_tree().set_current_scene( currentScene )
