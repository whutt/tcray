extends Node2D

var center
var radius
var innerRad
var nextPos

func _draw():	
	for i in range(6):
		var a = i * PI / 3.0
		var start = Vector2(innerRad, 0).rotated(a)
		var end = Vector2(radius, 0).rotated(a)
		
		draw_line(center + start, center + end, Color(0,0,0), 1.5, true)
	"""
	for i in range(8):
		var r = innerRad + i / 7.0 * (radius - innerRad)
		draw_arc(center, r, 0, 2.0 * PI, 61, Color(0,0,0), 1.01, true
	"""
	draw_arc(center, innerRad, 0, 2.0 * PI, 61, Color(0,0,0), 1.01, true)
	draw_arc(center, radius, 0, 2.0 * PI, 61, Color(0,0,0), 1.01, true)
	draw_arc(center, radius, 0, 2.0 * PI, 61, Color(0,0,0), 1.01, true)
	
	draw_arc(nextPos, innerRad, 0, 2.0 * PI, 31, Color(0,0,0), 1.01, true)
