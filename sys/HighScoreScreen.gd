extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _draw():
	var base = Vector2((misc.SCREEN_W - 24 * 6) / 2, 48)
	misc.drawSmall(self, base + Vector2(0, -8), "SCORE", Color(0.5, 0.5, 0.5))
	misc.drawSmall(self, base + Vector2(42, -8), "CLEARS", Color(0.5, 0.5, 0.5))
	misc.drawSmall(self, base + Vector2(90, -8), "TIME", Color(0.5, 0.5, 0.5))
	for i in range(10):
		var col = Color(0.75, 0.75, 1) if misc.highScores[i][3] == misc.lastGameID else Color(1,1,1)
		var score = str(misc.highScores[i][0])
		var clears = str(misc.highScores[i][1])
		misc.drawSmall(self, base + Vector2(6 * (6 - len(score)), i*8), score, col)
		misc.drawSmall(self, base + Vector2(42 + 6 * (6 - len(clears)), i*8), clears, col)
		misc.drawSmall(self, base + Vector2(90, i*8), misc.parseTime(misc.highScores[i][2]), col)
	
	var msg = "Press Z/X to return"
	misc.drawSmall(self, Vector2((misc.SCREEN_W - len(msg)*6)/2, misc.SCREEN_H-40), msg, Color(0.5, 0.5, 0.5))
	
func _process(_delta):
	for i in ["ui_accept","ui_cancel"]:
		if Input.is_action_just_pressed(i):
			misc.gotoScene("res://sys/Title.tscn")
