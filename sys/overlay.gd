extends Node2D

func _draw():
	var parent = get_parent()
	if parent.curState == parent.STATE_DEATH and parent.stateTime >= 0.5:
		#draw_rect(misc.screenRect, Color(0,0,0,pow((parent.stateTime - 0.5) * 0.5, 3.0)))
		misc.drawSmall(self, Vector2(misc.SCREEN_W/2 - 6 * 4.5, misc.SCREEN_H / 2 + 5), "GAME OVER")
