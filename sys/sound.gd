extends Node

var defaultVoice = AudioStreamPlayer.new()
var sounds = {}

var soundData = {}
onready var busIdx = AudioServer.get_bus_index("sfx")

func _ready():
	pause_mode = PAUSE_MODE_PROCESS
	var file = File.new()
	file.open("res://sys/sounds.txt", File.READ)
	
	while not file.eof_reached():
		# need to make this a function to stay DRY
		var line = file.get_line().strip_edges().replace("\t"," ").split(" ", false)
		
		if len(line) == 0 or line[0][0] == "#":
			continue
		
		soundData[line[0]] = {}
		var data = soundData[line[0]]
		data["volume"] = float(line[1])
	
	defaultVoice.name = "defaultVoice"
	defaultVoice.pause_mode = PAUSE_MODE_PROCESS
	add_child(defaultVoice)

func loadSound(soundName):
	var path = "res://sound/"+soundName+".wav"
	if ResourceLoader.exists(path):
		sounds[soundName] = load(path)
		return true
	else:
		print("sound ", path, " not found!")
		return false

func get(soundName):
	if not (soundName in sounds):
		if loadSound(soundName):
			return sounds[soundName]
		else:
			return null
	else:
		return sounds[soundName]

func getVolume(soundName):
	if not (soundName in soundData):
		return 0
	else:
		return soundData[soundName]["volume"]

func play(voice, soundName):
	if not voice:
		return
	if typeof(voice) == TYPE_STRING and voice == "default":
		voice = defaultVoice
	voice.set_bus("sfx") # just to make sure
	voice.stream = get(soundName)
	if not voice.stream:
		return
	voice.volume_db = getVolume(soundName)
	voice.play()
