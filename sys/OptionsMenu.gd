extends Menu

var accentCol = Color(0.85,0.85,1)

var keyDescs = {
	"ui_accept": "Confirm menu choices.",
	"ui_cancel": "Return to the previous menu.",
}

enum {
	PAD_STANDARD,
	PAD_XBOX,
	PAD_SONY,
	PAD_NINTENDO
}

var buttonDescs = [
	["A","B","X","Y","LB","RB","LT","RT","L Stick","R Stick","Back","Start"],
	["X","Circle","Square","Triangle","L1","R1","L2","R2","L3","R3","Select","Start"],
	["B","A","Y","X","L","R","? (sorry)","? (sorry)","? (sorry)","? (sorry)","? (sorry)","? (sorry)"]
]
var padKeywords = [
	["xinput"],
	[], [] # don't know keywords for these yet
]
var padMode = PAD_STANDARD

var oldCursor = 0

func ok_btnWindowSize():
	var value = getSetting("windowSize")
	settings.screenMult = settings.RES_AUTO if value == 0 else value
	settings.readjustResolution(false)

var settingsDict = {}

func getSetting(n):
	return settings.curSettings[n]

func _ready():
	optArray = []
	
	# detect what kind of pad we're using so we know what to name it
	var joyName = Input.get_joy_name(0)
	print("using pad: ", joyName)
	
	for i in len(padKeywords):
		if padMode > 0:
			break
		for k in padKeywords[i]:
			if joyName.to_lower().find(k) != -1:
				padMode = i+1
				break
	
	for i in settings.settingsList:
		optArray.append(i.name)
		i.desc = i.desc if not (i.type in [settings.OPT_JOYMAP, settings.OPT_KEYMAP]) else keyDescs[i.data]
		settingsDict[i.id] = i
		if i.type == settings.OPT_JOYMAP:
			i.value = settings.curSettings["joy_"+i.data]
		if i.type == settings.OPT_KEYMAP:
			i.value = settings.curSettings["key_"+i.data]
	
	var windowSize = settingsDict["windowSize"]
	var screenSize = OS.get_screen_size()
	screenSize.y -= 64 # should be enough to deal w/ window decorations etc.
	
	for i in range(min(int(screenSize.x / misc.SCREEN_W), int(screenSize.y / misc.SCREEN_H))):
		var fullRes = "%dx:%dx%d" % [i+1, misc.SCREEN_W * (i+1), misc.SCREEN_H * (i+1)]
		var pRes = "%dx (%dp)" % [i+1, misc.SCREEN_H * (i+1)]
		windowSize.data.append(fullRes if len(fullRes) <= 13 else pRes)
	
func onCursor():
	if settings.settingsList[cursor].type == settings.OPT_LABEL:
		var add = cursor - oldCursor
		jumpCursor(cursor+add)
	oldCursor = cursor

func onCancel():
	ok_btnWindowSize()
	settings.saveSettings()
	close()

func onSelect():
	var setting = settings.settingsList[cursor]
	if setting.type == settings.OPT_BUTTON and has_method("ok_"+setting.id):
		call("ok_"+setting.id)

func drawItem(i: int, j: int, base: Vector2):
	var setting = settings.settingsList[j]
	match setting.type:
		settings.OPT_LABEL:
			drawHeader(Vector2(base.x, base.y) + Vector2(0, (i+1)*itemSpace), setting.name)
		settings.OPT_BUTTON:
			drawItemBasic(i, j, base+Vector2(0, 0), setting.name, accentCol)
			#drawItemBasic(i, j, base, setting.name)
		_:
			drawItemBasic(i, j, base, setting.name)
			var base2 = base+Vector2(114, 0)
			var value = settings.curSettings[setting.id]
			match setting.type:
				settings.OPT_LIST:
					drawItemBasic(i, j, base2, setting.data[value], accentCol)
				settings.OPT_KEYMAP:
					drawItemBasic(i, j, base2, OS.get_scancode_string(value), accentCol)
				settings.OPT_JOYMAP:
					drawItemBasic(i, j, base2, "Button %d" % value if padMode == 0 else buttonDescs[padMode-1][value], accentCol)
				settings.OPT_SLIDER:
					var valSt = "%d%%" % (value)
					var baseAdd = Vector2(36 - len(valSt) * 3, 0)
					#var base3 = base2+Vector2(-4, (i+1)*itemSpace)
					#drawHeader(base3, valSt, accentCol)
					#draw_rect(Rect2(base3, Vector2(44, 4)), accentCol, false)
					drawItemBasic(i, j, base2+baseAdd, valSt, accentCol)
	#drawItemBasic(i, j, base+Vector2(120, 8), senderArray[j])

func onSideCursor(add):
	var setting = settings.settingsList[cursor]
	var curValue = settings.curSettings[setting.id]
	var newValue
	match setting.type:
		settings.OPT_SLIDER:
			newValue = max(0, min(curValue+add*5, 100))
		settings.OPT_LIST:
			newValue = wrapi(curValue+add, 0, len(setting.data))
	
	settings.curSettings[setting.id] = newValue
	
	if settings.has_method("change_"+setting.id):
		settings.call("change_"+setting.id, newValue)
	update()

func onDraw():
	var size = get_size()+Vector2(18,0)
	var topLeft = Vector2(16, 32)
	
	drawContents(topLeft, size)
	drawItemDescription(settings.settingsList[cursor])
