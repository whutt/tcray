extends Node2D

func _ready():
	call_deferred("reallyReady")

func reallyReady():
	var menu = load("res://sys/menu/TitleMenu.tscn").instance()
	menu.set_position(Vector2((misc.SCREEN_W - 46) / 2, (misc.SCREEN_H - 40) / 2))
	misc.callMenuLoaded(menu, misc.MENU_ROOT)
