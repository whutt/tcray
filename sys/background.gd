extends Node2D

var tex = load("res://sprites/background.png")
var time = 0.0
var colors = [Color(1,0,0,0.5), Color(0,1,0,0.5), Color(0,0,1,0.5)]
var speeds = [4.3, 5.4, 6.1]

func moveRect(r, x, y):
	return r.grow_individual(-x, -y, x, y)

func _ready():
	pause_mode = PAUSE_MODE_PROCESS

func _draw():
	for i in range(3):
		draw_texture_rect_region(tex, misc.screenRect, moveRect(misc.screenRect, i * 256 + time * speeds[i], i * 24), colors[i])

func _process(delta):
	tex.set_flags(Texture.FLAGS_DEFAULT)
	time += delta
	update()
